package app;

import java.util.Scanner;

public class ejercicio3 extends Thread {
    Scanner numero = new Scanner(System.in);
    int value;
    public ejercicio3(){
        System.out.print("Ingrese un Número: ");
        value=numero.nextInt();
    }
    @Override
    public void run() {
        int resultado = 1;
        for (int i = 1; i <= value; i++) {
            resultado *= i;
        }
        System.out.println("El factorial de "+value+" es: "+resultado); 
    }
    public static void main(String[] args) throws Exception {
        while (true) {
        Thread factorial = new ejercicio3();
        factorial.start();
        factorial.sleep(2000);
        }
    }
}